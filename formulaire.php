<!DOCTYPE html>
<html>
    <head>
        <title>Page de traitement</title>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Dans le formulaire précédent, vous avez fourni les
        informations suivantes :</p>
        
        <?php
            echo 'Titre du film : '.$_POST["nom_film"].'<br>';
            echo 'Affiche (url) : ' .$_POST["img_film"].'<br>';
            
        ?>


    <?php
    $serveur = "localhost";
    $dbname = "new_film";
    $user = "root";
    $pass = "";
    
    
    $name= $_POST['nom_film'];
    $url= $_POST['img_film'];
    
    try{
        //On se connecte à la BDD
        $dbco = new PDO("mysql:host=$serveur;dbname=$dbname",$user,$pass);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        //On insère les données reçues
        $sth = $dbco->prepare("
            INSERT INTO film(nom_film, img_film)
            VALUES(:nom_film, :img_film)");
        $sth->bindParam(':nom_film',$name);
        $sth->bindParam(':img_film',$url);
        $sth->execute();
        
        //On renvoie l'utilisateur vers la page de remerciement
        header("Location:index.php");
    }
    catch(PDOException $e){
        echo 'Impossible de traiter les données. Erreur : '.$e->getMessage();
    }
?>

        <button><a href=index.php>Revenir au menu</a></button>


    </body>
</html>